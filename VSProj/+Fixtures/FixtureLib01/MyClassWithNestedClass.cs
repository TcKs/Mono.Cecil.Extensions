﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MyClassWithNestedClass {
	public class MyNestedClass { }
}

namespace FixtureLib01 {
	public class MyClassWithNestedClass {
		public class MyNestedClass { }
	}
}
