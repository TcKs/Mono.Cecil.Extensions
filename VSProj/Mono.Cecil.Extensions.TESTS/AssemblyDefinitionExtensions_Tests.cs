﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mono.Cecil.ReflectionExtensions;

namespace Mono.Cecil.Extensions.TESTS {
	[TestClass]
	public class AssemblyDefinitionExtensions_Tests {
		[TestMethod]
		public void CreateAssemblyDefinition_From_MSCorLib_Works() {
			var asmDef = typeof(bool).Assembly.ToAssemblyDefinition();

			Assert.IsNotNull(asmDef);

			var tpDef = asmDef.EnumTypesRecursively().FirstOrDefault(x => x.EqualsByName(typeof(bool)));
			Assert.AreEqual("Boolean", tpDef.Name);
		}

		[TestMethod]
		public void EnumTypesRecursively_Returns_SameTypes_Like_Reflection() {
			var asm = typeof(bool).Assembly;
			var asmDef = asm.ToAssemblyDefinition();

			var types = asm.GetTypes();
			var typeDefs = asmDef.EnumTypesRecursively()
									//this is some internal types which are not provided by reflection
									.Where(x => x.FullName != "System.Runtime.Remoting.Proxies.__TransparentProxy" && x.FullName != "<Module>")
									.ToArray();

			var pairedTypeDefs = (from typeDef in typeDefs
								  from type in types
								  where typeDef.EqualsByName(type)
								  select typeDef).ToArray();

			// good for debug
			// var overlapingTypeDefs = typeDefs.Except(pairedTypeDefs).ToArray();

			Assert.AreEqual(types.Length, typeDefs.Length);
			Assert.AreEqual(types.Length, pairedTypeDefs.Length);
		}
	}
}
