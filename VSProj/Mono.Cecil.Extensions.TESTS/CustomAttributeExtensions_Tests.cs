﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mono.Cecil.ReflectionExtensions;

namespace Mono.Cecil.Extensions.TESTS {
	[TestClass]
	public class CustomAttributeExtensions_Tests {
		[TestMethod]
		public void IsOfType_TypeReference_Works_WithAttributesFromMSCorLib() {
			var asmDef = typeof(bool).Assembly.ToAssemblyDefinition();
			var typeDefs = asmDef.EnumTypesRecursively().ToArray();

			foreach (var typeDef in typeDefs) {
				this.IsOfType_TypeReference_Works(typeDef);

				foreach (var memberDef in typeDef.EnumMemberDefinitions()) {
					this.IsOfType_TypeReference_Works(memberDef);
				}
			}
		}

		private void IsOfType_TypeReference_Works(ICustomAttributeProvider parent) {
			foreach (var attrDef in parent.CustomAttributes) {
				var attrTypeDef = attrDef.AttributeType;
				var rslt = attrDef.IsOfType(attrTypeDef);
				Assert.IsTrue(rslt);
			}
		}
	}
}
