﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mono.Cecil.Extensions.TESTS {
	public static class MyAssert {
		public static Exception Throws(Action action, Action<Exception> assertAction) {
			return Throws(action, (string)null, assertAction);
		}
		public static Exception Throws(Action action, string message, Action<Exception> assertAction) {
			if (object.ReferenceEquals(action, null)) { throw new ArgumentNullException(nameof(action)); }

			Exception thrownException = null;
			try { action(); }
			catch (Exception exc) { thrownException = exc; }

			var finalMessage = "Unhandled exception was expected.";
			if (!string.IsNullOrWhiteSpace(message)) {
				finalMessage += $" {message}";
			}
			Assert.IsNotNull(thrownException, finalMessage);

			assertAction?.Invoke(thrownException);

			return thrownException;
		}
	}
}
