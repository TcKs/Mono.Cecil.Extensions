﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mono.Cecil.Extensions.TESTS {
	[TestClass]
	public class TypeDefinitionExtensions_Tests {
		[TestMethod]
		public void EnumTypesRecursively_Throws_ArgumentNullException() {
			MyAssert.Throws(
				() => TypeDefinitionExtensions.EnumTypesRecursively(null).ToArray()
				, (thrownException) => {
					var exc = (ArgumentNullException)thrownException;
					Assert.AreEqual("self", exc.ParamName);
				});
		}
	}
}
