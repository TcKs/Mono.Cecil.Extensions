﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mono.Cecil.ReflectionExtensions;

namespace Mono.Cecil.Extensions.TESTS {
	[TestClass]
	public class TypeReferenceExtensions_Tests {
		[TestMethod]
		public void EqualsByName_Works_With_Class_WithoutNamespace() {
			var asmDef = typeof(MyEmptyClass01).Assembly.ToAssemblyDefinition();

			{
				var type = typeof(MyEmptyClass01);
				var typeDef = asmDef.EnumTypesRecursively().FirstOrDefault(x => x.EqualsByName(type));
				Assert.IsNotNull(typeDef);
				Assert.AreEqual(string.Empty, typeDef.Namespace);
				Assert.AreEqual(type.Name, typeDef.Name);
			}

			{
				var type = typeof(MyEmptyClass02);
				var typeDef = asmDef.EnumTypesRecursively().FirstOrDefault(x => x.EqualsByName(type));
				Assert.IsNotNull(typeDef);
				Assert.AreEqual(string.Empty, typeDef.Namespace);
				Assert.AreEqual(type.Name, typeDef.Name);
			}

			{
				var type = typeof(FixtureLib01.MyEmptyClass01);
				var typeDef = asmDef.EnumTypesRecursively().FirstOrDefault(x => x.EqualsByName(type));
				Assert.IsNotNull(typeDef);
				Assert.AreEqual(type.Namespace, typeDef.Namespace);
				Assert.AreEqual(type.Name, typeDef.Name);
				Assert.IsNull(typeDef.DeclaringType);
			}

			{
				var type = typeof(FixtureLib01.MyEmptyClass01);
				var typeDef = asmDef.EnumTypesRecursively().FirstOrDefault(x => x.EqualsByName(type));
				Assert.IsNotNull(typeDef);
				Assert.AreEqual(type.Namespace, typeDef.Namespace);
				Assert.AreEqual(type.Name, typeDef.Name);
				Assert.IsNull(typeDef.DeclaringType);
			}

			{
				var type = typeof(MyClassWithNestedClass.MyNestedClass);
				var typeDef = asmDef.EnumTypesRecursively().FirstOrDefault(x => x.EqualsByName(type));
				Assert.IsNotNull(typeDef);
				Assert.AreEqual(string.Empty, typeDef.Namespace);
				Assert.AreEqual(type.Name, typeDef.Name);

				var declaringType = type.DeclaringType;
				var declaringTypeDef = typeDef.DeclaringType;
				Assert.IsNotNull(declaringType);
				Assert.IsNotNull(declaringTypeDef);
				Assert.AreEqual(declaringType.Namespace ?? string.Empty, declaringTypeDef.Namespace);
				Assert.AreEqual(declaringType.Name, declaringTypeDef.Name);

				Assert.IsNull(declaringTypeDef.DeclaringType);
			}

			{
				var type = typeof(FixtureLib01.MyClassWithNestedClass.MyNestedClass);
				var typeDef = asmDef.EnumTypesRecursively().FirstOrDefault(x => x.EqualsByName(type));
				Assert.IsNotNull(typeDef);
				Assert.AreEqual(string.Empty, typeDef.Namespace);
				Assert.AreEqual(type.Name, typeDef.Name);

				var declaringType = type.DeclaringType;
				var declaringTypeDef = typeDef.DeclaringType;
				Assert.IsNotNull(declaringType);
				Assert.IsNotNull(declaringTypeDef);
				Assert.AreEqual(declaringType.Namespace ?? string.Empty, declaringTypeDef.Namespace);
				Assert.AreEqual(declaringType.Name, declaringTypeDef.Name);

				Assert.IsNull(declaringTypeDef.DeclaringType);
			}
		}
	}
}
