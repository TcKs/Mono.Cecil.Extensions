﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.ReflectionExtensions {
	/// <summary>
	/// Extension methods for <c>System.Reflection.Assembly</c>.
	/// </summary>
	public static class AssemblyExtensions {
		/// <summary>
		/// Returns new <c>Mono.Cecil.AssemblyDefinition</c> created from <c>self</c>.
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static AssemblyDefinition ToAssemblyDefinition(this Assembly self) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }

			return AssemblyDefinition.ReadAssembly(self.Location);
		}
	}
}
