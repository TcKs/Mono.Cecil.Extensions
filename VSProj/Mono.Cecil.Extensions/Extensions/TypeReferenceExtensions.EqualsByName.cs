﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	partial class TypeReferenceExtensions {
		public static bool EqualsByName(this TypeReference self, Type type) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }
			if (object.ReferenceEquals(type, null)) { throw new ArgumentNullException(nameof(type)); }

			var comparer = StringComparer.Ordinal;
			if (0 != comparer.Compare(self.Name, type.Name)) { return false; }
			// nested classes has empty namespace in Mono.Cecil.TypeReference

			var selfDeclaringType = self.DeclaringType;
			var isSelfDeclarintTypeNull = object.ReferenceEquals(selfDeclaringType, null);

			var typeDeclaringType = type.DeclaringType;
			var isTypeDeclaringTypeNull = object.ReferenceEquals(typeDeclaringType, null);

			if (isSelfDeclarintTypeNull && isTypeDeclaringTypeNull) {
				// only non-nested classes has defined namespace in Mono.Cecil.TypeReference
				if (0 != comparer.Compare(self.Namespace, type.Namespace ?? string.Empty)) { return false; }

				return true;
			}
			if (isSelfDeclarintTypeNull != isTypeDeclaringTypeNull) { return false; }

			return EqualsByName(selfDeclaringType, typeDeclaringType);
		}
	}
}
