﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	partial class TypeReferenceExtensions {
		public static TypeDefinition ResolveOrNull(this TypeReference self) {
			Exception thrownException;
			return ResolveOrNull(self, out thrownException);
		}
		public static TypeDefinition ResolveOrNull(this TypeReference self, out Exception thrownException) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }

			thrownException = null;
			try { return self.Resolve(); }
			catch (Exception exc) { thrownException = exc; }

			return null;
		}
	}
}
