﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	/// <summary>
	/// Extensions methods for <c>Mono.Cecil.ModuleDefinition</c>.
	/// </summary>
	public static class ModuleDefinitionExtensions {
		/// <summary>
		/// Enumerates all types defined in <paramref name="self"/>.
		/// </summary>
		/// <param name="self">Module with types to return.</param>
		/// <exception cref="ArgumentNullException">Thrown if <paramref name="self"/> is null.</exception>
		/// <returns></returns>
		public static IEnumerable<TypeDefinition> EnumTypesRecursively(this ModuleDefinition self) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }

			foreach (var type in self.Types) {
				yield return type;

				foreach (var subType in type.EnumTypesRecursively()) {
					yield return subType;
				}
			}
		}
	}
}
