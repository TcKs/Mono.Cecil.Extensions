﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	/// <summary>
	/// Extension methods for <c>Mono.Cecil.TypeDefinition</c>.
	/// </summary>
	public static partial class TypeDefinitionExtensions { }
}
