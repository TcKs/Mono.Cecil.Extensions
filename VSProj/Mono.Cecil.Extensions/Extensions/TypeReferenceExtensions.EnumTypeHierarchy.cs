﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	partial class TypeReferenceExtensions {
		public static IEnumerable<TypeReference> EnumTypeHierarchy(this TypeReference self) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }

			yield return self;

			var selfDef = self.ResolveOrNull();
			if (object.ReferenceEquals(selfDef, null)) { yield break; }

			var baseType = selfDef.BaseType;
			if (!object.ReferenceEquals(baseType, null)) {
				foreach (var type in EnumTypeHierarchy(baseType)) {
					yield return type;
				}
			}

			foreach (var implementedType in selfDef.Interfaces) {
				foreach (var type in EnumTypeHierarchy(implementedType)) {
					yield return type;
				}
			}
		}
	}
}
