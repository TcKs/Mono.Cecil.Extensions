﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	partial class CustomAttributeExtensions {
		public static bool IsOfType(this CustomAttribute self, TypeReference type) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }
			if (object.ReferenceEquals(type, null)) { throw new ArgumentNullException(nameof(type)); }

			var selfFullName = self.AttributeType?.FullName ?? string.Empty;
			var typeFullName = type.FullName ?? string.Empty;

			return (0 == StringComparer.Ordinal.Compare(selfFullName, typeFullName));
		}
	}
}
