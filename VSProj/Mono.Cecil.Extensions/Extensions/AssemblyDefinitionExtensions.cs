﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	/// <summary>
	/// Extensions methods for <c>Mono.Cecil.AssemblyDefinition</c>.
	/// </summary>
	public static class AssemblyDefinitionExtensions {
		/// <summary>
		/// Enumerates all types defined in <paramref name="self"/>.
		/// </summary>
		/// <param name="self">Assembly with types to return.</param>
		/// <exception cref="ArgumentNullException">Thrown if <paramref name="self"/> is null.</exception>
		/// <returns></returns>
		public static IEnumerable<TypeDefinition> EnumTypesRecursively(this AssemblyDefinition self) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }

			foreach (var module in self.Modules) {
				foreach (var type in module.EnumTypesRecursively()) {
					yield return type;
				}
			}
		}
	}
}
