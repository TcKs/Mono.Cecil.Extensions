﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	partial class TypeDefinitionExtensions {
		/// <summary>
		/// Enumerates all types defined in <paramref name="self"/>.
		/// </summary>
		/// <param name="self">Type with types to return.</param>
		/// <exception cref="ArgumentNullException">Thrown if <paramref name="self"/> is null.</exception>
		/// <returns></returns>
		public static IEnumerable<TypeDefinition> EnumTypesRecursively(this TypeDefinition self) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }

			foreach (var type in self.NestedTypes) {
				yield return type;

				foreach (var subType in type.EnumTypesRecursively()) {
					yield return subType;
				}
			}
		}
	}
}
