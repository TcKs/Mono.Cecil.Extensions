﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mono.Cecil.Extensions {
	partial class TypeDefinitionExtensions {
		public static IEnumerable<IMemberDefinition> EnumMemberDefinitions(this TypeDefinition self) {
			if (object.ReferenceEquals(self, null)) { throw new ArgumentNullException(nameof(self)); }

			foreach (var field in self.Fields) {
				yield return field;
			}
			foreach (var method in self.Methods) {
				yield return method;
			}
			foreach (var property in self.Properties) {
				yield return property;
			}
			foreach (var @event in self.Events) {
				yield return @event;
			}
		}
	}
}
